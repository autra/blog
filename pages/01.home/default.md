---
title: Home
body_classes: 'title-center title-h1h2'
---

Hi there! I'm **Augustin Trancart**. I write open-source software. 

I maintain [py3dtiles](https://py3dtiles.org/v7.0.1-alpha.1/) and [giro3d](https://girod.org).

I'm currently a Senior GIS Engineer at [Oslandia](https://oslandia.com). 

You can find me on [twitter](https://twitter.com/autra42) or matrix (@autra:trancart.eu). My [github](https://github.com/autra), but most of my stuff are now on [gitlab](https://gitlab.com/autra).