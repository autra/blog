---
title: 'Werkzeug needs to be pinned on old version of flask (<= 0.12)'
published: true
taxonomy:
    tag:
        - python
        - pip
        - sysadmin
media_order: pexels-kindel-media-8326329.jpg
hero_image: pexels-kindel-media-8326329.jpg
hero_classes: hero-tiny
---

I have encountered this issue several times already, including one time where I was called by a *former* customer. Enough of this, let's document this:

## The issue: an old flask (<= 0.12) suddenly stopped working

This flask app was working fine for some time already. Maybe you tried to update some of the code, maybe you re-run your ansible scripts, whatever: you tried to restart it and it fails.

It looks like this:

```
Error: The file/path provided (flask_app) does not appear to exist. Please verify the path is correct. If app is not on PYTHONPATH, ensure the extension is .py
```

So you try to correct your `FLASK_APP` environment variable, but after several minutes spent and a bit less hair, you cannot seem to find any solution.

## Bad flask logs badly

Yeah. It's because the root cause is not the value in `FLASK_APP`. It's fine, you did it correctly. If you are in a good day, you had the good idea to google it, and you've found [this](https://stackoverflow.com/a/41913346/1528985). Yeah, the error is deeper, a failed import somewhere in your codebase.

## Bad flask pins badly

At this point, the more experienced in maintaining python (or even js) application might already have an idea, but let's not spoil it. The best way to debug this is to (optionnally activate your venv,) fire up a python console and try to import your application by hand.

And then you have [this](https://github.com/jarus/flask-testing/issues/143#issuecomment-591903005):

> cannot import name 'cached_property' from 'werkzeug'

And here you are : old versions of Flask doesn't pin werkzeug version correctly. Werkzeug is not compatible with these old flask versions after v1.0.

The short-term solution is to pin it to `v0.16.1`.

The long-term solution: update your flask dependency. If you use `flask-restplus`, migrate to a maintained fork: `flask-restx`.

Hope this helps :-)

## Final note

By the way, I quite like Flask and still use it daily which doesn't prevent me to rage from time to time!
